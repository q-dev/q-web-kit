# Q React Hooks

A set of React hooks for Q Blockchain frontend projects.

<div>
  
  [![npm](https://img.shields.io/npm/v/@q-dev/react-hooks?color=ca0001)](https://www.npmjs.com/package/@q-dev/react-hooks)
  [![npm min zipped size](https://img.shields.io/bundlephobia/minzip/@q-dev/react-hooks)](https://bundlephobia.com/package/@q-dev/react-hooks@1.4.0)
  [![license](https://img.shields.io/npm/l/@q-dev/react-hooks)](https://gitlab.com/q-dev/q-web-kit/-/blob/main/LICENSE)
  [![pipeline status](https://gitlab.com/q-dev/q-web-kit/badges/main/pipeline.svg)](https://gitlab.com/q-dev/q-web-kit/-/commits/main)
</div>

## Installation

To install the library, run the following command:

```bash
yarn add @q-dev/react-hooks
```

or

```bash
npm install @q-dev/react-hooks
```

### Requirements

* [react ^17.0.1](https://npmjs.com/package/react)

## Usage

### Importing

```jsx
import { useInterval } from '@q-dev/react-hooks'

export default function App() {
  const [count, setCount] = useState(0)

  useInterval(() => {
    setCount(count + 1)
  }, 1000)

  return <div>{count}</div>
}
```

### Hooks

| Hook | Description |
| --- | --- |
| `useAnimateNumber` | Animate number from one value to another. |
| `useChangesListener` | Listen for changes in the value. |
| `useCopyToClipboard` | Copy text to clipboard. |
| `useEventCallback` | useCallback for events. |
| `useEventListener` | Event listener handlers. |
| `useInfinityNumber` | Infinity number animation. |
| `useInterval` | Call a function at a specified interval. |
| `useLocalStorage` | Manage local storage. |
| `useOnClickOutside` | Handle click outside of the element. |
| `useOnScreen` | Detect if the element is visible on the screen. |
| `useWindowSize` | Get window size. |

##  Contribution

We welcome contributions to the library. If you would like to submit a pull request, please make sure to follow our code style.

## Code of Conduct

This project and everyone participating in it is governed by the
[Q React Hooks Code of Conduct](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code.

## Resources

* [Changelog](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CHANGELOG.md)
* [Contributing Guide](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CONTRIBUTING.md)

## License

[LGPL-3.0](https://gitlab.com/q-dev/q-web-kit/-/blob/main/LICENSE)
