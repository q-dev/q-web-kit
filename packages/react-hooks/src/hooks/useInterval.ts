import { useEffect, useRef } from 'react';

interface IntervalOpts {
  immediate?: boolean;
  disabled?: boolean;
}

function useInterval (
  callback: () => void,
  delay: number,
  { immediate = false, disabled = false }: IntervalOpts = {}
) {
  const savedCallback = useRef<() => void>(callback);

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    if (disabled) return;

    const interval = setInterval(() => savedCallback.current(), delay);
    return () => clearInterval(interval);
  }, [delay, disabled]);

  useEffect(() => {
    if (!immediate) return;
    savedCallback.current();
  }, [immediate]);
}

export default useInterval;
