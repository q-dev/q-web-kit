import { BigNumber as BigNumberBase } from 'bignumber.js';
import isNumber from 'lodash/isNumber';

export const BigNumber = BigNumberBase.clone();

export function toBigNumber (value: BigNumberBase.Value): BigNumberBase {
  return new BigNumber(value);
}

export function parseNumber (value: string): number {
  return Number(value.toString().replace(/[Q,%]/g, ''));
}

export function formatNumber (value: BigNumberBase.Value, precision = 4): string {
  return toBigNumber(value).decimalPlaces(precision, BigNumber.ROUND_DOWN).toFormat();
}

export function formatNumberFixed (value: BigNumberBase.Value, precision = 4): string {
  return toBigNumber(value).toFormat(precision);
}

export function formatNumberCompact (value: BigNumberBase.Value, precision = 4): string {
  const rounded = toBigNumber(value).decimalPlaces(precision, BigNumber.ROUND_DOWN);

  return Intl.NumberFormat('en-US', {
    notation: 'compact',
    maximumFractionDigits: precision,
  }).format(Number(rounded));
}

export function formatAsset (value: BigNumberBase.Value, asset = ''): string {
  return `${formatNumber(value)} ${asset}`.trim();
}

export function getFixedPercentage (value: BigNumberBase.Value) {
  return toBigNumber('1e+25').multipliedBy(value).toFixed();
}

export function transformToPercentage (value: BigNumberBase.Value): string {
  return toBigNumber(value).dividedBy('10000000000000000000000000').toFixed(6);
}

export function formatPercent (value: BigNumberBase.Value, precision?: number): string {
  return isNaN(Number(value.toString())) ? '0 %' : `${formatNumber(value, precision)} %`;
}

export function formatFraction (value: BigNumberBase.Value): string {
  return formatPercent(transformToPercentage(value));
}

export function formatFactor (value: BigNumberBase.Value): string {
  const divider = toBigNumber(10).pow(27);
  return formatNumber(toBigNumber(value).div(divider).toString());
}

export function calculateInterestRate (value: number): number {
  if (!isNumber(value) || isNaN(value)) return 0;

  const SECONDS_IN_YEAR = 365 * 24 * 60 * 60;
  const MAX_INTEREST = 10 ** 27;

  const ratePerSecond = value >= MAX_INTEREST ? 100 : Math.max(value / MAX_INTEREST, 0);

  return ((1 + ratePerSecond) ** SECONDS_IN_YEAR - 1) * 100;
}
