import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import resolve from '@rollup/plugin-node-resolve';
import terser from '@rollup/plugin-terser';
import typescript from '@rollup/plugin-typescript';
import nodePolyfills from 'rollup-plugin-polyfill-node';

const packageDirName = __dirname.split('/').pop();

export default {
  input: `${__dirname}/src/index.ts`,
  output: {
    sourcemap: true,
    file: `${__dirname}/dist/index.js`,
    name: `Q_${packageDirName}`,
    format: 'iife',
    extend: true,
  },
  plugins: [
    commonjs(),
    resolve({
      browser: true,
      preferBuiltins: false,
    }),
    nodePolyfills(),
    babel({
      babelHelpers: 'bundled',
      exclude: [`${__dirname}/src/tests`, `${__dirname}/src/*.test.ts`],
    }),
    typescript({
      tsconfig: `${__dirname}/tsconfig.json`,
    }),
    json({ compact: true, }),
    terser()
  ]
};
