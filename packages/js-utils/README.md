# Q JS Utils

A set of JavaScript utils for Q Blockchain frontend projects.

<div>
  
  [![npm](https://img.shields.io/npm/v/@q-dev/utils?color=ca0001)](https://www.npmjs.com/package/@q-dev/utils)
  [![npm min zipped size](https://img.shields.io/bundlephobia/minzip/@q-dev/utils)](https://bundlephobia.com/package/@q-dev/utils@1.4.0)
  [![license](https://img.shields.io/npm/l/@q-dev/utils)](https://gitlab.com/q-dev/q-web-kit/-/blob/main/LICENSE)
  [![pipeline status](https://gitlab.com/q-dev/q-web-kit/badges/main/pipeline.svg)](https://gitlab.com/q-dev/q-web-kit/-/commits/main)
</div>

## Installation

To install the library, run the following command:

```bash
yarn add @q-dev/utils
```

or

```bash
npm install @q-dev/utils
```

## Usage

### Importing

```js
import { dateToUnix } from '@q-dev/utils'

dateToUnix(new Date())
```

### Util functions

* `arrays` - array utils (fill array)
* `date` - date utils (converters, formatters, etc.)
* `numbers` - number utils (converters, formatters, interest rate, etc.)
* `strings` - string utils (trim string, case converters, etc.)

##  Contribution

We welcome contributions to the library. If you would like to submit a pull request, please make sure to follow our code style.

## Code of Conduct

This project and everyone participating in it is governed by the
[Q JS Utils Code of Conduct](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code.

## Resources

* [Changelog](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CHANGELOG.md)
* [Contributing Guide](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CONTRIBUTING.md)

## License

[LGPL-3.0](https://gitlab.com/q-dev/q-web-kit/-/blob/main/LICENSE)
