import { HTMLAttributes } from 'react';

import { TagContainer } from './styles';
import { TagState } from '.';

interface Props extends HTMLAttributes<HTMLDivElement> {
  state: TagState
}

function Tag ({
  state,
  children,
  className,
  ...rest
}: Props) {
  return (
    <TagContainer
      className={`q-ui-tag text-md ${className || ''}`}
      $state={state}
      {...rest}
    >
      {children}
    </TagContainer>
  );
};

export default Tag;
