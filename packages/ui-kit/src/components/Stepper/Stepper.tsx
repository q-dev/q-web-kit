import { HTMLAttributes } from 'react';

import { StepperContainer } from './styles';

import Icon from '@/components/Icon';

interface Props extends HTMLAttributes<HTMLDivElement> {
  current: number
  steps: { id: string, name: string }[]
}

function Stepper ({ current, steps, className, ...rest }: Props) {
  return (
    <StepperContainer className={`q-ui-stepper ${className || ''}`} {...rest}>
      {steps.map((step, index) => (
        <div key={step.id} className="q-ui-stepper__step">
          <div
            className={`
              q-ui-stepper__step-check
              ${index < current ? 'q-ui-stepper__step-check--passed' : ''}
              ${index === current ? 'q-ui-stepper__step-check--current' : ''}
          `}
          >
            <Icon
              className="q-ui-stepper__step-check-icon"
              name="check"
            />
          </div>

          <div className="q-ui-stepper__step-content">
            <p className="q-ui-stepper__step-index text-md">
              {/* TODO: change to locale */}
              { `Step ${index + 1}` }
            </p>
            <p className="q-ui-stepper__step-name text-xl">
              {step.name}
            </p>
          </div>
        </div>
      ))}
    </StepperContainer>
  );
};

export default Stepper;
