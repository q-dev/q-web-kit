import { HTMLAttributes, ReactNode } from 'react';

import uniqueId from 'lodash/uniqueId';

import { RadioContainer } from './styles';

type ValueType = number | string | boolean
interface Props<T extends ValueType> extends Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> {
  checked: boolean
  value: T
  name: string
  label?: ReactNode
  tip?: string
  extended?: boolean
  disabled?: boolean
  onChange: (value: T) => void
};

function Radio<T extends ValueType> ({
  checked,
  value,
  name,
  label,
  tip,
  extended = false,
  disabled = false,
  onChange,
  className,
  ...rest
}: Props<T>) {
  const inputId = `q-ui-radio__${uniqueId()}`;

  return (
    <RadioContainer
      className={`q-ui-radio ${className || ''}`}
      $checked={checked}
      $disabled={disabled}
      $extended={extended}
      {...rest}
    >
      <input
        id={inputId}
        className="q-ui-radio__input"
        type="radio"
        name={name}
        value={String(value)}
        checked={checked}
        disabled={disabled}
        onChange={() => onChange(value)}
      />

      <div className="q-ui-radio__frame">
        <div className="q-ui-radio__circle" />
      </div>

      <label
        htmlFor={inputId}
        className={`q-ui-radio__label ${extended ? 'text-lg font-semibold' : 'text-md'}`}
      >
        {label}
        {tip && !extended && (
          <span className="q-ui-radio__tip text-md font-light">{tip}</span>
        )}
      </label>

      {extended && tip && (
        <span className="q-ui-radio__tip text-md">
          {tip}
        </span>
      )}
    </RadioContainer>
  );
};

export default Radio;
