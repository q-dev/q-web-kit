import { HTMLAttributes } from 'react';

import { motion } from 'framer-motion';
import uniqueId from 'lodash/uniqueId';

import { SwitchContainer } from './styles';

interface Props extends Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> {
  value: boolean
  label: string
  disabled?: boolean
  onChange: (val: boolean) => void
}

function Switch ({
  value,
  label,
  disabled = false,
  onChange,
  className,
  ...rest
}: Props) {
  const inputId = `q-ui-switch__${uniqueId()}`;

  return (
    <SwitchContainer
      className={`q-ui-switch ${className || ''}`}
      $checked={value}
      $disabled={disabled}
      {...rest}
    >
      <label
        htmlFor={inputId}
        className="q-ui-switch__label text-lg"
      >
        {label}
      </label>

      <input
        id={inputId}
        className="q-ui-switch__input"
        type="checkbox"
        checked={value}
        disabled={disabled}
        onChange={() => onChange(!value)}
      />

      <div className="q-ui-switch__background">
        <motion.div
          layout
          className="q-ui-switch__circle"
          transition={{
            type: 'spring',
            stiffness: 700,
            damping: 30,
          }}
        />
      </div>
    </SwitchContainer>
  );
};

export default Switch;
