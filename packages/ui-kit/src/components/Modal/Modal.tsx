import { ReactNode } from 'react';
import { createPortal } from 'react-dom';
import { useHotkeys } from 'react-hotkeys-hook';

import { AnimatePresence, HTMLMotionProps } from 'framer-motion';

import { ModalContainer } from './styles';

import Button from '@/components/Button';
import Icon from '@/components/Icon';

interface Props extends Omit<HTMLMotionProps<'div'>, 'title'> {
  open: boolean;
  title: ReactNode;
  tip?: string;
  width?: number;
  onClose: () => void;
}

function Modal ({
  open,
  title,
  tip,
  width = 420,
  children,
  onClose,
  className,
  ...rest
}: Props) {
  useHotkeys('esc', () => onClose());

  return (
    createPortal((
      <AnimatePresence>
        {open && (
          <ModalContainer
            className={`q-ui-modal ${className || ''}`}
            $width={width}
            transition={{ duration: 0.2 }}
            variants={{
              open: { opacity: 1 },
              closed: { opacity: 0 },
            }}
            initial="closed"
            animate={open ? 'open' : 'closed'}
            exit="closed"
            {...rest}
          >
            <div className="q-ui-modal__overlay" onClick={onClose} />
            <div className="q-ui-modal__dialog block">
              <Button
                icon
                className="q-ui-modal__close"
                look="ghost"
                onClick={onClose}
              >
                <Icon name="cross" />
              </Button>

              <h3 className="q-ui-modal__title text-h2">
                {title}
              </h3>

              {tip && (
                <p className="q-ui-modal__tip text-md">
                  {tip}
                </p>
              )}

              <div className="q-ui-modal__content">
                {children}
              </div>
            </div>
          </ModalContainer>
        )}
      </AnimatePresence>
    ), document.body)
  );
}

export default Modal;
