import { InputHTMLAttributes } from 'react';

import { SearchContainer } from './styles';

import Button from '@/components/Button';
import Icon from '@/components/Icon';

interface Props extends Omit<InputHTMLAttributes<HTMLInputElement>, 'onChange'> {
  value: string
  disabled?: boolean
  onChange: (val: string) => void
}

function Search ({
  value,
  disabled,
  onChange,
  className,
  ...rest
}: Props) {
  return (
    <SearchContainer className={`q-ui-search__container ${className || ''}`} $disabled={disabled}>
      <Icon className="q-ui-search__icon" name="search" />
      <input
        className="q-ui-search__input text-md"
        value={value}
        type="search"
        autoComplete="off"
        disabled={disabled}
        onChange={(e) => onChange((e.target as HTMLInputElement).value)}
        {...rest}
      />

      {value && !disabled && (
        <Button
          icon
          className="q-ui-search__reset"
          look="ghost"
          onClick={() => onChange('')}
        >
          <Icon name="cross" />
        </Button>
      )}
    </SearchContainer>
  );
};

export default Search;
