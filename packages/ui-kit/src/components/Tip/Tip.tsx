import { HTMLAttributes, ReactNode } from 'react';

import { TipWrapper } from './styles';
import { TipType } from '.';

import Icon, { IconName } from '@/components/Icon';

interface Props extends HTMLAttributes<HTMLDivElement> {
  type?: TipType
  compact?: boolean
  action?: ReactNode
}

function Tip ({
  type = 'info',
  compact = false,
  action,
  children,
  className,
  ...rest
}: Props) {
  const typeToIcon: Record<TipType, IconName> = {
    info: 'info',
    warning: 'warning',
  };

  return (
    <TipWrapper
      className={`q-ui-tip ${className || ''}`}
      $type={type}
      $compact={compact}
      {...rest}
    >
      <Icon name={typeToIcon[type]} className="q-ui-tip__icon" />
      <div className="q-ui-tip__text text-md">{children}</div>
      <div className="q-ui-tip__action">{action}</div>
    </TipWrapper>
  );
}

export default Tip;
