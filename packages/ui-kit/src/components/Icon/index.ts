import { icons } from './icons';

export { default } from './Icon';
export type IconName = keyof typeof icons
