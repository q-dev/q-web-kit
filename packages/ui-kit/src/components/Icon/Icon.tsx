import { HTMLAttributes } from 'react';

import { icons } from './icons';
import { StyledIcon } from './styles';
import { IconName } from '.';

interface Props extends HTMLAttributes<HTMLSpanElement> {
  name: IconName
}

function Icon ({ name, className, ...rest }: Props) {
  return (
    <StyledIcon
      className={`q-ui-icon ${className || ''}`}
      $content={icons[name]}
      {...rest}
    />
  );
}

export default Icon;
