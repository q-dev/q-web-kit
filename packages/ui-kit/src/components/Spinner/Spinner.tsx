import { HTMLAttributes } from 'react';

import { StyledSpinner } from './styles';

interface Props extends HTMLAttributes<SVGSVGElement> {
  size?: number;
  thickness?: number;
}

function Spinner ({
  size = 20,
  thickness = 2,
  className,
  ...rest
}: Props) {
  return (
    <StyledSpinner
      className={`q-ui-spinner ${className}`}
      $size={size}
      $radius={size / 2 - 2 * thickness}
      {...rest}
    >
      <circle
        className="q-ui-spinner__circle"
        cx={size / 2}
        cy={size / 2}
        r={size / 2 - 2 * thickness}
        fill="none"
        strokeWidth={thickness}
      />
    </StyledSpinner>
  );
}

export default Spinner;
