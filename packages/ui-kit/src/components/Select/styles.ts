import styled from 'styled-components';

export const SelectContainer = styled.div<{
  $open: boolean
  $disabled?: boolean
}>`
  .q-ui-select__arrow {
    display: flex;
    padding: 0;
    border-radius: 4px;
    cursor: ${({ $disabled }) => $disabled ? 'not-allowed' : 'pointer'};
    outline: none;
    border: none;
    background-color: transparent;
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.iconPrimary
    };

    &:focus-visible {
      box-shadow: inset 0 0 0 2px ${({ theme }) => theme.colors.primaryLight};
    }
  }

  .q-ui-select__chips {
    border-radius: 8px;
  }

  .q-ui-select__icon {
    transform: ${({ $open }) => $open ? 'rotate(180deg)' : 'none'};
    transition: transform 150ms ease-out;
  }

  .q-ui-select__options {
    background: ${({ theme }) => theme.colors.backgroundPrimary};
    box-shadow:
      0px 4px 4px  ${({ theme }) => theme.colors.shadowMain},
      0px -1px 2px ${({ theme }) => theme.colors.shadowMain};
    border: 1px solid ${({ theme }) => theme.colors.borderSecondary};
    border-radius: 8px;
    padding: 4px 0;
    display: grid;
    overflow-y: auto;
    max-height: 264px;
  }

  .q-ui-select__option {
    padding: 8px;
    cursor: pointer;
    border: none;
    background-color: transparent;
    text-align: left;
    display: flex;
    align-items: center;
    outline: none;
    gap: 8px;
    color: ${({ theme, $disabled }) => $disabled
       ? theme.colors.disableSecondary
       : theme.colors.textPrimary
    };

    &:hover {
      background-color: ${({ theme }) => theme.colors.tertiaryLight};
    }

    &:focus-visible {
      box-shadow: inset 0 0 0 2px ${({ theme }) => theme.colors.primaryLight};
    }

    &--active {
      background-color: ${({ theme }) => theme.colors.tertiaryMain};
    }
  }

  .q-ui-select__option-icon {
    opacity: 0;

    &--active {
      opacity: 1;
    }
  }

  .q-ui-select__stub {
    padding: 16px;
    color: ${({ theme }) => theme.colors.textSecondary};
  }

  .q-ui-select__error,
  .q-ui-select__hint {
    margin-top: 4px;
  }

  .q-ui-select__error {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.errorMain
    };
  }

  .q-ui-select__hint {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };
  }
`;
