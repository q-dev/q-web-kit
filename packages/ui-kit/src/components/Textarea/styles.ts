
import styled, { css } from 'styled-components';

export const TextareaWrapper = styled.div<{
  $error?: string,
  $disabled?: boolean,
}>`
  width: 100%;

  .q-ui-textarea__label-wrp {
    margin-bottom: 8px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .q-ui-textarea__label {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };
  }

  .q-ui-textarea__label-counter {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };
  }

  .q-ui-textarea__container {
    padding: 12px 16px;
    display: flex;
    align-items: center;
    gap: 8px;
    border-radius: 8px;
    transition: all 100ms ease-out;
    cursor: ${({ $disabled }) => $disabled ? 'not-allowed' : 'initial'};
    background-color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disablePrimary
      : 'transparent'
    };
    border: 1px solid ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.borderTertiary
    };
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };

    ${({ $disabled }) => !$disabled && css`
      &:hover {
        border-color: ${({ theme }) => theme.colors.borderAdditional};
      }
    
      &:focus-within {
        border-color: ${({ theme }) => theme.colors.primaryMain};
      }
    `}

    ${({ $error, $disabled }) => !$disabled && $error && css`
      &,
      &:focus-within,
      &:hover {
        border-color: ${({ theme }) => theme.colors.errorMain};
      }
    `}
  }

  .q-ui-textarea__textarea {
    padding: 0;
    border: none;
    text-align: left;
    width: 100%;
    background-color: inherit;
    color: inherit;
    outline: none;
    resize: none;

    &::placeholder {
      color: ${({ theme, $disabled }) => $disabled
        ? theme.colors.disableSecondary
        : theme.colors.textTertiary
      };
    }

    &:disabled {
      -webkit-text-fill-color: ${({ theme }) => theme.colors.disableSecondary};
      cursor: not-allowed;
    }
  }


  .q-ui-textarea__extra {
    margin-right: -4px;
    display: flex;
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.iconSecondary
    };
  }

  .q-ui-textarea__error,
  .q-ui-textarea__hint {
    margin-top: 4px;
  }

  .q-ui-textarea__error {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.errorMain
    };
  }

  .q-ui-textarea__hint {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };
  }
`;
