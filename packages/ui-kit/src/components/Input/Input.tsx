import {
  ChangeEvent,
  HTMLInputTypeAttribute,
  InputHTMLAttributes,
  ReactNode,
  useEffect,
  useRef,
  useState
} from 'react';

import isNil from 'lodash/isNil';
import uniqueId from 'lodash/uniqueId';

import { InputWrapper } from './styles';

import Button from '@/components/Button';

type ThousandsSeparator = ',' | '_' | '-' | '/'

function formatNumWithSeparator (n: string, separator: ThousandsSeparator) {
  const isFloat = n.includes('.');
  const [numSplit, decimalSplit] = n.split('.');
  const thousands = /\B(?=(\d{3})+(?!\d))/g;
  return numSplit.replace(thousands, separator) + (isFloat ? '.' : '') + (isFloat && decimalSplit ? decimalSplit : '');
}

type InputProps = InputHTMLAttributes<HTMLInputElement>
interface Props extends Omit<InputProps, 'onChange' | 'prefix' | 'value'> {
  value: string | number | boolean
  label?: ReactNode
  error?: string
  hint?: string
  disabled?: boolean
  type?: HTMLInputTypeAttribute
  max?: string
  decimals?: number;
  numberSeparator?: ThousandsSeparator;
  prefix?: ReactNode
  children?: ReactNode
  labelTip?: string;
  onChange: (val: string) => void
}

function Input ({
  value,
  label,
  error,
  type = 'text',
  disabled,
  hint,
  max,
  decimals = 18,
  numberSeparator,
  prefix,
  labelTip,
  children,
  className,
  onChange = () => {},
  ...rest
}: Props) {
  const [cursor, setCursor] = useState<number | null>(null);
  const ref = useRef<HTMLInputElement>(null);

  const inputId = `q-ui-input__${uniqueId()}`;

  const handleChange = (e: ChangeEvent) => {
    setCursor((e.target as HTMLInputElement).selectionStart);
    const value = (e.target as HTMLInputElement).value;

    if (type === 'number') {
      const numbersRegexp = decimals
        ? new RegExp(`^[0-9]{1,50}[.]?[0-9]{0,${decimals}}$`)
        : /^[0-9]{1,50}$/;

      const normalizedValue = numberSeparator ? value.replaceAll(numberSeparator, '') : value;
      const isNumberValid = value === '' || numbersRegexp.test(normalizedValue);
      if (!isNumberValid) return;

      onChange(normalizedValue);
      return;
    }

    onChange(value);
  };

  const formattedVal = type === 'number' && numberSeparator
    ? formatNumWithSeparator(String(value), numberSeparator)
    : String(value);

  useEffect(() => {
    ref.current?.setSelectionRange(cursor, cursor);
  }, [ref, cursor, value]);

  return (
    <InputWrapper
      className={`q-ui-input ${className || ''}`}
      $error={error}
      $disabled={disabled}
      $type={type}
    >
      {label && (
        <div className="q-ui-input__label-wrp">
          <label
            htmlFor={inputId}
            className="q-ui-input__label text-md"
          >
            {label}
          </label>
          {labelTip && (
            <span className="text-sm font-light">
              {labelTip}
            </span>
          )}
        </div>
      )}

      <div className="q-ui-input__container">
        {prefix && (
          <div className="q-ui-input__prefix text-md font-semibold">
            {prefix}
          </div>
        )}
        <input
          ref={ref}
          id={inputId}
          className="q-ui-input__input text-md"
          value={formattedVal}
          type={type === 'number' ? 'text' : type}
          inputMode={type === 'number' ? 'decimal' : 'text'}
          autoComplete="off"
          disabled={disabled}
          onChange={handleChange}
          {...rest}
        />

        {children && <div className="q-ui-input__extra">{children}</div>}
        {!isNil(max) && !children && (
          <Button
            look="ghost"
            disabled={disabled}
            className="q-ui-input__max text-sm font-semibold"
            onClick={() => onChange(max)}
          >
            Max
          </Button>
        )}
      </div>

      {error && (
        <span className="q-ui-input__error text-md font-light">{error}</span>
      )}

      {hint && !error && (
        <span className="q-ui-input__hint text-md font-light">{hint}</span>
      )}
    </InputWrapper>
  );
};

export default Input;
