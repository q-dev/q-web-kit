import { HTMLInputTypeAttribute } from 'react';

import styled, { css } from 'styled-components';

export const InputWrapper = styled.div<{
  $error?: string,
  $disabled?: boolean,
  $type: HTMLInputTypeAttribute
}>`
  width: 100%;

  .q-ui-input__label-wrp {
    margin-bottom: 8px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .q-ui-input__label {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };
  }

  .q-ui-input__container {
    padding: 12px 16px;
    display: flex;
    align-items: center;
    gap: 8px;
    border-radius: 8px;
    transition: all 100ms ease-out;
    cursor: ${({ $disabled }) => $disabled ? 'not-allowed' : 'initial'};
    background-color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disablePrimary
      : 'transparent'
    };
    border: 1px solid ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.borderTertiary
    };
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };

    ${({ $disabled }) => !$disabled && css`
      &:hover {
        border-color: ${({ theme }) => theme.colors.borderAdditional};
      }
    
      &:focus-within {
        border-color: ${({ theme }) => theme.colors.primaryMain};
      }
    `}

    ${({ $error, $disabled }) => !$disabled && $error && css`
      &,
      &:focus-within,
      &:hover {
        border-color: ${({ theme }) => theme.colors.errorMain};
      }
    `}
  }

  .q-ui-input__prefix {
    white-space: nowrap;
  }

  .q-ui-input__input {
    padding: 0;
    border: none;
    text-align: left;
    width: 100%;
    background-color: inherit;
    color: inherit;
    outline: none;

    &::placeholder {
      color: ${({ theme, $disabled }) => $disabled
        ? theme.colors.disableSecondary
        : theme.colors.textTertiary
      };
    }

    &:disabled {
      -webkit-text-fill-color: ${({ theme }) => theme.colors.disableSecondary};
      cursor: not-allowed;
    }
  }

  .q-ui-input__max {
    margin: -4px -8px -4px 0;
    padding: 0 8px;
    height: 28px;
    white-space: nowrap;
  }

  .q-ui-input__extra {
    margin-right: -4px;
    display: flex;
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.iconSecondary
    };
  }

  .q-ui-input__error,
  .q-ui-input__hint {
    margin-top: 4px;
  }

  .q-ui-input__error {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.errorMain
    };
  }

  .q-ui-input__hint {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };
  }
`;
