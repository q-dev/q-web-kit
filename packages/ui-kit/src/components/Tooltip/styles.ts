import styled, { css } from 'styled-components';

export const TooltipWrapper = styled.div<{ $disabled: boolean }>`
  display: inline-flex;
  align-self: center;

  .q-ui-tooltip__trigger {
    display: flex;
    cursor: ${({ $disabled }) => $disabled ? 'inherit' : 'help'};
  }

  .q-ui-tooltip__content {
    position: absolute;
    opacity: 0;
    z-index: 9999;
    pointer-events: none;
    padding: 12px;
    padding: 12px;
    background-color: ${({ theme }) => theme.colors.tertiaryMain};
    color: ${({ theme }) => theme.colors.textPrimary};
    max-width: 280px;
    width: max-content;
    border-radius: 8px;
    transition: none;

    .q-ui-tooltip__arrow,
    .q-ui-tooltip__arrow::before,
    .q-ui-tooltip__arrow::after {
      position: absolute;
      width: 10px;
      height: 10px;
      background-color: inherit;
    }

    .q-ui-tooltip__arrow {
      visibility: hidden;
    }

    .q-ui-tooltip__arrow::before,
    .q-ui-tooltip__arrow::after {
      visibility: visible;
      content: '';
    }

    .q-ui-tooltip__arrow::before {
      transform: rotate(45deg);
    }

    .q-ui-tooltip__arrow::after {
      background-color: transparent;
      width: 20px;
      height: 8px;
    }

    &[data-popper-placement^='top'] > .q-ui-tooltip__arrow {
      bottom: -4px;
      
      &::before {
        border-radius: 0 0 2px 0;
      }

      &::after {
        transform: translate(-4px, 4px);
      }
    }

    &[data-popper-placement^='bottom'] > .q-ui-tooltip__arrow {
      top: -4px;
      
      &::before {
        border-radius: 2px 0 0 0;
      }

      &::after {
        transform: translate(-4px, -2px);
      }
    }

    &[data-popper-placement^='left'] > .q-ui-tooltip__arrow {
      right: -4px;

      &::before {
        border-radius: 0 2px 0 0;
      }

      &::after {
        transform: rotate(90deg);
      }
    }

    &[data-popper-placement^='right'] > .q-ui-tooltip__arrow {
      left: -4px;

      &::before {
        border-radius: 0 0 0 2px;
      }

      &::after {
        transform: translateX(-8px) rotate(90deg);
      }
    }
  }

  ${({ $disabled }) => !$disabled && css`
    &:hover .q-ui-tooltip__content {
      opacity: 1;
      pointer-events: all;
      overflow: visible;
      transition: opacity 200ms ease-out;

      &[data-popper-escaped],
      &[data-popper-reference-hidden] {
        opacity: 0;
        pointer-events: none;
      }
    }
  `}
`;
