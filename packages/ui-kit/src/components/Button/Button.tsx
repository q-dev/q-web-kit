import { HTMLAttributes, MouseEventHandler } from 'react';

import { StyledButton } from './styles';

import Spinner from '@/components/Spinner';

export type ButtonLook = 'primary' | 'secondary' | 'ghost' | 'danger';

interface Props extends HTMLAttributes<HTMLButtonElement> {
  type?: 'button' | 'submit' | 'reset';
  look?: ButtonLook;
  disabled?: boolean;
  icon?: boolean;
  compact?: boolean;
  loading?: boolean;
  active?: boolean;
  block?: boolean;
  onClick?: (e: MouseEventHandler<HTMLButtonElement> | any) => void;
}

function Button ({
  type = 'button',
  look = 'primary',
  disabled = false,
  icon = false,
  compact = false,
  loading = false,
  active = false,
  block = false,
  children,
  className,
  onClick = () => {},
  ...rest
}: Props) {
  return (
    <StyledButton
      className={`q-ui-button text-md font-semibold ${className || ''}`}
      as={block ? 'div' : 'button'}
      type={type}
      disabled={disabled}
      $look={look}
      $icon={icon}
      $compact={compact}
      loading={String(loading)}
      data-active={String(active)}
      tabIndex={loading || block ? -1 : 0}
      onClick={onClick}
      {...rest}
    >
      {loading && <Spinner />}
      {children}
    </StyledButton>
  );
}

export default Button;
