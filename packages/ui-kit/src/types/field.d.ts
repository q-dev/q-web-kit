import { ReactNode } from 'react';

export type FieldOption<T> = {
  value: T
  label: string
}
export type RadioOption<T, L = ReactNode> = {
  value: T
  label: L
}
export type FieldOptions<T> = FieldOption<T>[];
export type RadioOptions<T, L = ReactNode> = (RadioOption<T, L> & { tip?: string, disabled?: boolean })[]
