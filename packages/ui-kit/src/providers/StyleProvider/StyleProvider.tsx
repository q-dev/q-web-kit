import { ReactNode, useMemo } from 'react';

import { ThemeProvider } from 'styled-components';

import { useLocalStorage } from '@/hooks';
import {
  DEFAULT_DARK_COLORS,
  DEFAULT_LIGHT_COLORS,
  FontStyle,
  GlobalStyle,
  ResetStyle,
  TextStyle,
  THEMES
} from '@/styles';
import { Colors } from '@/types';

interface Props {
  children: ReactNode;
  darkColors?: Partial<Colors>;
  lightColors?: Partial<Colors>;
}

function StyleProvider ({ children, darkColors, lightColors }: Props) {
  const [theme, setTheme] = useLocalStorage('theme', THEMES.dark);

  const isDarkTheme = theme === THEMES.dark;

  const currentDarkColors = useMemo(() => {
    return darkColors ? { ...DEFAULT_DARK_COLORS, ...darkColors } : DEFAULT_DARK_COLORS;
  }, [darkColors]);

  const currenLightColors = useMemo(() => {
    return lightColors ? { ...DEFAULT_LIGHT_COLORS, ...lightColors } : DEFAULT_LIGHT_COLORS;
  }, [lightColors]);

  const handleChangeTheme = () => {
    setTheme(isDarkTheme ? THEMES.light : THEMES.dark);
  };

  return (
    <ThemeProvider
      theme={{
        currentTheme: theme,
        isDarkTheme,
        colors: isDarkTheme ? currentDarkColors : currenLightColors,
        onChangeTheme: handleChangeTheme,
      }}
    >
      <FontStyle />
      <ResetStyle />
      <GlobalStyle />
      <TextStyle />
      {children}
    </ThemeProvider>
  );
}

export default StyleProvider;
