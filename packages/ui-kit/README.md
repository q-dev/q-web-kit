<div align="center"><img src="img/q-logo.png" width="128"></div>
<div align="center"><h1>Q UI Kit</h1></div>
<div align="center">
  
  **[Examples](https://gitlab.com/q-dev/q-web-kit/-/tree/main/packages/ui-kit/src/example)** •
  **[Your HQ](https://gitlab.com/q-dev/dapp)** •
  **[Q Blockchain](https://q.org)**
</div>
<div align="center"><p>A set of UI components for Q Blockchain</p></div>
<div align="center">
  
  [![npm](https://img.shields.io/npm/v/@q-dev/q-ui-kit?color=ca0001)](https://www.npmjs.com/package/@q-dev/q-ui-kit)
  [![npm min zipped size](https://img.shields.io/bundlephobia/minzip/@q-dev/q-ui-kit)](https://bundlephobia.com/package/@q-dev/q-ui-kit@1.4.0)
  [![license](https://img.shields.io/npm/l/@q-dev/q-ui-kit)](https://gitlab.com/q-dev/q-web-kit/-/blob/main/LICENSE)
  [![pipeline status](https://gitlab.com/q-dev/q-web-kit/badges/main/pipeline.svg)](https://gitlab.com/q-dev/q-web-kit/-/commits/main)
</div>

## Installation

To install the library, run the following command:

```bash
yarn add @q-dev/q-ui-kit
```

or

```bash
npm install @q-dev/q-ui-kit
```

### Requirements

* [bignumber.js ^9.1.0](https://npmjs.com/package/bignumber.js)
* [framer-motion ^4.1.17](https://npmjs.com/package/framer-motion)
* [react ^17.0.1](https://npmjs.com/package/react)
* [react-dom ^17.0.1](https://npmjs.com/package/react-dom)
* [styled-components ^5.3.5](https://npmjs.com/package/styled-components)

To install all dependencies, run the following command:

```bash
yarn add bignumber.js@^9.0.1 framer-motion@^4.1.17 react@^17.0.1 react-dom@^17.0.1 styled-components@^5.3.5
```

## Usage

### Style provider

The library uses [styled-components](https://styled-components.com/) for styling. To make sure that the styles are applied correctly, you need to wrap your app in the `StyleProvider` component:

```jsx
import { StyleProvider } from '@q-dev/q-ui-kit';

const App = () => (
  <StyleProvider>
    <MyApp />
  </StyleProvider>
);
```

### Importing components

To use a component, import it from the library:

```js
import { Button } from '@q-dev/q-ui-kit';
```

### Using components

To use a component, simply add it to your JSX:

```html
<Button>Click me</Button>
```

### Styling components

To override the default styles, you can use component-scoped class names (starting with `q-ui-`):
  
```css
.q-ui-button {
  background-color: red;
}
```

### Changing the theme

To change the theme, you can use the `useTheme` hook:

```jsx
import { useTheme } from 'styled-components';

const App = () => {
  const { isDarkTheme, onChangeTheme } = useTheme();

  return (
    <div>
      <p>Current theme: {isDarkTheme ? 'dark' : 'light'}</p>
      <button onClick={() => onChangeTheme(!isDarkTheme)}>Change theme</button>
    </div>
  );
};
```

##  Contribution

We welcome contributions to the library. If you would like to submit a pull request, please make sure to follow our code style.

## Code of Conduct

This project and everyone participating in it is governed by the
[Q UI Kit Code of Conduct](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code.

## Resources

* [Changelog](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CHANGELOG.md)
* [Contributing Guide](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CONTRIBUTING.md)

## License

[LGPL-3.0](https://gitlab.com/q-dev/q-web-kit/-/blob/main/LICENSE)
