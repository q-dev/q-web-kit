import react from '@vitejs/plugin-react';
import fs from 'fs';
import path from 'path';
import { defineConfig } from 'vite';
import dts from 'vite-plugin-dts';

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = (relative: string) => path.resolve(appDirectory, relative);

const root = path.resolve(__dirname, resolveApp('src'));

export default defineConfig(() => {
  return {
    plugins: [
      dts({
        insertTypesEntry: true,
      }),
      react(),
    ],
    optimizeDeps: {
      esbuildOptions: {
        define: { global: 'globalThis' },
      },
    },
    resolve: {
      preserveSymlinks: true,
      alias: {
        '@': `${root}/`,
      },
    },
    build: {
      lib: {
        entry: `${root}/index.ts`,
        name: 'QUiKit',
        formats: ['es', 'umd'],
        fileName: 'q-ui-kit',
      },
      rollupOptions: {
        external: ['react', 'react-dom', 'styled-components'],
        output: {
          globals: {
            react: 'React',
            'react-dom': 'ReactDOM',
            'styled-components': 'styled',
          },
        },
      },
    },
  };
});
