# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Feature 1
- Feature 2

### Changed
- Changelog generation

### Fixed
- Some bugs
- Other bugs

## [0.2.2] - 2022-10-31
### Changed
- Props on Range

## [0.2.1] - 2022-09-08
### Changed
- `framer-motion` version

## [0.2.0] - 2022-09-05
### Added
- Components
- Hooks
- Providers
- Styles
- Types
- Utils

## 0.1.0 - 2022-09-02
### Added
- Initiated project

[Unreleased]: https://gitlab.com/q-dev/changelog-cli/compare/v0.2.2...main
[0.2.2]: https://gitlab.com/q-dev/changelog-cli/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/q-dev/changelog-cli/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/q-dev/changelog-cli/compare/v0.1.0...v0.2.0
