import fs from 'fs';

import { Changelog } from '../src/changelog';

const mockChangelog = 'tests/mocks/_CHANGELOG.md';
const origin = 'https://gitlab.com/q-dev/changelog-cli';

describe('Changelog', () => {
  describe('getLatestRelease', () => {
    it('Should get the latest release', () => {
      const changelog = new Changelog(mockChangelog, origin);
      const release = changelog.getLatestRelease();

      expect(release?.toString()).toEqual([
        '## 0.2.2 - 2022-10-31',
        '### Changed',
        '- Props on Range',
      ].join('\n'));
    });
  });

  describe('getRelease', () => {
    it('Should get a release by version', () => {
      const changelog = new Changelog(mockChangelog, origin);
      const release = changelog.getRelease('0.2.0');

      expect(release?.toString()).toEqual([
        '## 0.2.0 - 2022-09-05',
        '### Added',
        '- Components',
        '- Hooks',
        '- Providers',
        '- Styles',
        '- Types',
        '- Utils',
      ].join('\n'));
    });

    it('Should get a release by version with a v prefix', () => {
      const changelog = new Changelog(mockChangelog, origin);
      const release = changelog.getRelease('v0.2.0');

      expect(release?.toString()).toEqual([
        '## 0.2.0 - 2022-09-05',
        '### Added',
        '- Components',
        '- Hooks',
        '- Providers',
        '- Styles',
        '- Types',
        '- Utils',
      ].join('\n'));
    });
  });

  describe('release', () => {
    const version = '0.3.0';
    const tmpChangelog = 'tests/mocks/_CHANGELOG.md.tmp';
    const emptyChangelog = 'tests/mocks/_EMPTY_CHANGELOG.md';

    beforeEach(() => {
      jest
        .useFakeTimers()
        .setSystemTime(new Date('2022-10-31T10:20:30Z'));
      fs.copyFileSync(mockChangelog, tmpChangelog);
    });

    afterEach(() => {
      fs.unlinkSync(tmpChangelog);
      jest.useRealTimers();
    });

    it('Should make a release', () => {
      const changelog = new Changelog(tmpChangelog, origin);

      const release = changelog.makeRelease(version);
      const latestRelease = changelog.getLatestRelease();

      expect(release.toString())
        .toEqual(latestRelease?.toString());

      expect(release.toString()).toEqual([
        '## 0.3.0 - 2022-10-31',
        '### Added',
        '- Feature 1',
        '- Feature 2',
        '',
        '### Changed',
        '- Changelog generation',
        '',
        '### Fixed',
        '- Some bugs',
        '- Other bugs',
      ].join('\n'));
    });

    it('Should throw an error if "Unreleased" section is empty', () => {
      const changelog = new Changelog(emptyChangelog, origin);

      expect(() => changelog.makeRelease(version))
        .toThrow('Error! [Unreleased] section of CHANGELOG.md is empty.');
    });
  });
});
