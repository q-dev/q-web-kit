# Changelog CLI

[![pipeline status](https://gitlab.com/q-dev/q-web-kit/badges/main/pipeline.svg)](https://gitlab.com/q-dev/q-web-kit/-/commits/main) [![Latest Release](https://gitlab.com/q-dev/q-web-kit/-/badges/release.svg)](https://gitlab.com/q-dev/q-web-kit/-/releases)

CLI tool for [changelog](https://keepachangelog.com/en/1.0.0/) automation.

## Installation

### Install globally

```bash
npm i -g @q-dev/changelog-cli
```

or

```bash
yarn add --global @q-dev/changelog-cli
```

### Iinstall as a dev dependency

```bash
npm i -D @q-dev/changelog-cli
```

or

```bash
yarn add -D @q-dev/changelog-cli
```

## Usage

Release changes from `[Unreleased]`

### Release version from package.json

```bash
changelog-cli release
```

### Release specific version

```bash
changelog-cli release 1.0.1
```

### Get release changes

#### Get latest release changes

```bash
changelog-cli get latest
```

#### Get specific release

```bash
changelog-cli get 1.0.1
```

### Use custom changelog location

```bash
changelog-cli -f ./nested/CHANGELOG.md get latest
```
