import { readFileSync, writeFileSync } from 'fs';
import { Changelog as IChangelog, parser, Release } from 'keep-a-changelog';
import get from 'lodash/get';

import { getDefaultBranch, getOriginURL, normalizeOriginURL } from './git-helpers';

export class Changelog {
  private _filePath: string;
  private _changelog: IChangelog;

  constructor (filePath: string, originURL: string, head?: string) {
    this._filePath = filePath;
    this._changelog = parser(readFileSync(filePath, { encoding: 'utf-8' }));
    this._changelog.url = originURL;
    this._changelog.tagNameBuilder = release => `v${release.version}`;
    this._changelog.head = head || 'master';
  }

  static async create (filePath: string, head?: string): Promise<Changelog> {
    const [originURL, defaultBranch] = await Promise.all([
      head || getOriginURL(),
      getDefaultBranch()
    ]);

    return new Changelog(filePath, normalizeOriginURL(head || originURL), defaultBranch);
  }

  makeRelease (version: string): Release {
    const currentRelease = this._changelog.releases[0];
    if (!this._containsChanges(currentRelease)) {
      throw new Error('Error! [Unreleased] section of CHANGELOG.md is empty.');
    }

    // Make a release
    currentRelease.setVersion(version);
    currentRelease.setDate(new Date());

    // Add empty [Unreleased] section
    this._changelog.addRelease(new Release());

    writeFileSync(this._filePath, this._changelog.toString());

    return currentRelease;
  }

  getRelease (version: string): Release | undefined {
    version = version.replace(/^v/, '');

    return this._changelog.releases.find((release) => {
      return get(release, 'version.version') === version;
    });
  }

  getLatestRelease (): Release | undefined {
    return this._changelog.releases
      .find((release) => release.date && release.version);
  }

  private _containsChanges (release: Release): boolean {
    return Array.from(release.changes.values())
      .reduce((acc, changes) => acc || changes.length > 0, false);
  }
}
