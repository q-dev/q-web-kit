declare module 'keep-a-changelog' {
  export function parser (changelog: string): Changelog;
  export class Change {
    text: string;
    constructor (text: string);
  }
  export class Version {
    version: string;
    constructor (version: string);
  }
  export class Release {
    version: Version;
    date: Date;
    changes: Map<string, Change[]>;
    setVersion (version: string): void;
    setDate (date: Date): void;
  }
  export class Changelog {
    releases: Release[];
    url: string;
    tagNameBuilder: (release: Release) => string;
    head: string;
    addRelease (release: Release): void;
    toString (): string;
  }
}
