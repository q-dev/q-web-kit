#!/usr/bin/env node

import { program } from 'commander';

import { Changelog } from '.';

const DEFAULT_FILENAME = 'CHANGELOG.md';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const pkgVersion = require(`${process.cwd()}/package.json`).version;

// eslint-disable-next-line @typescript-eslint/no-var-requires
const cliVersion = require('../package.json').version;

program.version(cliVersion)
  .option('-f, --file <path>', 'Custom CHANGELOG.md location', DEFAULT_FILENAME)
  .option('-o, --origin-url <url>', 'Custom repository origin URL');

program
  .command('release [version]')
  .description('Make a release from [Unreleased] section')
  .action(async (version) => {
    try {
      const changelog = await Changelog.create(program.file, program.originUrl);
      const release = changelog.makeRelease(version || pkgVersion);
      console.info(release.toString());
    } catch (err) {
      console.error(err);
      process.exit(1);
    }
  });

program
  .command('validate')
  .description('Validate changelog')
  .action(async () => {
    try {
      await Changelog.create(program.file, program.originUrl);
      console.info('Changelog is valid');
    } catch (err) {
      console.error(err);
      process.exit(1);
    }
  });

program
  .command('get <version>')
  .description('Get changes in a specific release. Use "latest" for the latest release')
  .action(async (version) => {
    try {
      const changelog = await Changelog.create(program.file, program.originUrl);
      const release = version === 'latest'
        ? changelog.getLatestRelease()
        : changelog.getRelease(version);

      if (!release) {
        throw new Error(`Release ${version} not found`);
      }

      console.info(release.toString());
    } catch (err) {
      console.error(err);
      process.exit(1);
    }
  });

program.parse(process.argv);
