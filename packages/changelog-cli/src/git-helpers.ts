import { simpleGit } from 'simple-git';

const git = simpleGit();

export async function getOriginURL () {
  const remote = await git.remote(['get-url', 'origin']);
  return remote?.trim() || '';
}

export async function getDefaultBranch () {
  const branch = await git.raw(['rev-parse', '--abbrev-ref', 'origin/HEAD']);
  return branch?.trim().replace('origin/', '') || '';
}

export function normalizeOriginURL (url: string) {
  return url.replace(/git@(.+):(.+)\.git/, 'https://$1/$2');
}
