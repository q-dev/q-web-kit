import useForm from '../hooks/useForm';

export type Form<T> = ReturnType<
  typeof useForm<Extract<keyof T, string>, T[keyof T]>
>;

export type ValidationResultType =
  'email' |
  'required' |
  'amount-greater-zero' |
  'available-amount-zero' |
  'max-amount' |
  'min-value' |
  'max-value' |
  'url' |
  'address' |
  'zero-address' |
  'vault-id' |
  'hash' |
  'current-hash' |
  'future-date' |
  'percent'

export interface ValidationResult {
  isValid: boolean;
  message: string;
  type: ValidationResultType;
}
export type ValidatorValue = string | number | boolean | Date | File | null;
export type Validator<T extends ValidatorValue = ValidatorValue, U = unknown> = (val: T, form?: U) => ValidationResult;
export type ValidatorFn<U, T extends ValidatorValue = ValidatorValue> =
  (val: U | ((form: unknown) => T)) => Validator<T>;
