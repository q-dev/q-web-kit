import { useCallback, useState } from 'react';

interface MultiStepFormProps<T> {
  initialValues?: T;
  onConfirm?: (_: T) => void;
}

export default function useMultiStepForm<T> ({
  initialValues = {} as T,
  onConfirm = (_: T) => {},
}: MultiStepFormProps<T>) {
  const [stepIndex, setStepIndex] = useState(0);
  const [values, setValues] = useState({ ...initialValues });

  const goNext = (values?: Partial<T>) => {
    onChange(values || {} as T);
    setStepIndex(prev => prev + 1);
  };

  const onChange = useCallback((values?: Partial<T>) => {
    setValues((prev) => ({ ...prev, ...values }));
  }, [setValues]);

  const goBack = () => {
    setStepIndex(prev => prev - 1);
  };

  const confirm = () => {
    onConfirm(values);
  };

  const reset = () => {
    setStepIndex(0);
    setValues({ ...initialValues });
  };

  return {
    stepIndex,
    values,
    updateStep: setStepIndex,
    onChange,
    goNext: useCallback(goNext, [onChange, setStepIndex]),
    goBack: useCallback(goBack, [setStepIndex]),
    confirm: useCallback(confirm, [values, onConfirm]),
    reset: useCallback(reset, [setStepIndex, setValues])
  };
}
