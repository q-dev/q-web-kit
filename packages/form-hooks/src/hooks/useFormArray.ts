import { useCallback, useMemo, useState } from 'react';

import uniqueId from 'lodash/uniqueId';

import { Form } from '../types';

function useFormArray<T> ({
  minCount = 0,
  maxCount = Infinity,
  initCount,
  onSubmit = () => {}
}: {
  minCount?: number;
  maxCount?: number;
  initCount?: number;
  onSubmit?: (values: T[]) => void;
}) {
  const [forms, setForms] = useState(getInitialForms(initCount));
  const [isSubmitting, setIsSubmitting] = useState(false);

  function getInitialForms (count?: number) {
    return new Array(count ?? minCount).fill(null).map(createForm);
  }

  function createForm () {
    const id = uniqueId();
    const onChange = (form: Form<T>) => {
      setForms((prev) => prev.map(e => {
        return e.id === id ? { ...e, ...form } : e;
      }));
    };

    return { id, onChange } as { id: string; onChange: (form: Form<T>) => void } & Form<T>;
  };

  const validate = () => {
    return forms.map((form) => form.validate()).every((val) => val);
  };

  const submit = async (e?: Event) => {
    e?.preventDefault();
    if (!validate()) return;

    setIsSubmitting(true);
    await onSubmit(forms.map(e => e.values) as T[]);
    setIsSubmitting(false);
  };

  const reset = (count?: number) => {
    setForms(getInitialForms(count));
  };

  const appendForm = () => {
    if (forms.length >= maxCount) return;
    setForms((prev) => [...prev, createForm()]);
  };

  const removeForm = (id: string) => {
    if (forms.length <= minCount) return;
    setForms((prev) => prev.filter((e) => e.id !== id));
  };

  return {
    forms,
    isSubmitting,
    isValid: useMemo(() => {
      return forms.every((e) => e.isValid);
    }, [forms]),

    validate: useCallback(validate, [forms]),
    submit: useCallback(submit, [forms, onSubmit]),
    reset: useCallback(reset, [forms]),

    appendForm: useCallback(appendForm, [forms]),
    removeForm: useCallback(removeForm, [forms])
  };
}

export default useFormArray;
