import { validate as validateEmail } from 'email-validator';
import isBoolean from 'lodash/isBoolean';
import isDate from 'lodash/isDate';
import isEmpty from 'lodash/isEmpty';
import isNumber from 'lodash/isNumber';

import { Validator, ValidatorFn, ValidatorValue } from '../types';
import { toBigNumber } from '../utils/numbers';

const HASH_REGEX = /^0x[a-fA-F0-9]{64}$/;
const VAULT_ID_REGEX = /^[0-9]{1,18}$/;
export const URL_REGEX = /^https?:\/\/(www\.)?[-äöüa-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-äöüa-zA-Z0-9()@:%_+.~#?&//=]*)/;

export const email: Validator = (val) => ({
  isValid: !val || validateEmail(String(val)),
  message: 'Invalid email',
  type: 'email',
});

export const required: Validator = (val) => ({
  isValid: !isEmpty(val) || isNumber(val) || isDate(val) || isBoolean(val) || val instanceof File,
  message: 'The field is required',
  type: 'required',
});

export const requiredIf: ValidatorFn<(val: ValidatorValue, form: unknown) => boolean> = predicate => (val, form) => {
  return {
    isValid: !predicate(val, form) || required(val).isValid,
    message: 'The field is required',
    type: 'required'
  };
};

export const amount: ValidatorFn<string | number> = max => (val, form) => {
  const value = toBigNumber(String(val));
  const zero = toBigNumber(0);
  const validatorValue = toBigNumber(String(getValidatorValue(max, form)));

  if (value.comparedTo(zero) === 0) {
    return {
      isValid: false,
      message: 'Amount must be greater than 0',
      type: 'amount-greater-zero'
    };
  }

  if (validatorValue.comparedTo(toBigNumber(0)) === 0) {
    return {
      isValid: false,
      message: 'Available amount is 0',
      type: 'available-amount-zero'
    };
  }

  return {
    isValid: value.comparedTo(validatorValue) <= 0,
    message: `Max amount: ${max}`,
    type: 'max-amount'
  };
};

export const min: ValidatorFn<string | number> = min => (val, form) => {
  const value = toBigNumber(String(val));
  const validatorValue = toBigNumber(String(getValidatorValue(min, form)));

  return {
    isValid: value.comparedTo(validatorValue) >= 0,
    message: `Minimum value is ${min}`,
    type: 'min-value'
  };
};

export const max: ValidatorFn<string | number> = max => (val, form) => {
  const value = toBigNumber(String(val));
  const validatorValue = toBigNumber(String(getValidatorValue(max, form)));

  return {
    isValid: value.comparedTo(validatorValue) <= 0,
    message: `Maximum value is ${max}`,
    type: 'max-value'
  };
};

export const url: Validator = val => ({
  isValid: !val || URL_REGEX.test(String(val)),
  message: 'Invalid URL',
  type: 'url'
});

export const vaultID: Validator = val => ({
  isValid: !val || VAULT_ID_REGEX.test(String(val)),
  message: 'Invalid vault ID',
  type: 'vault-id'
});

export const hash: Validator = val => ({
  isValid: !val || HASH_REGEX.test(String(val)),
  message: 'Invalid hash',
  type: 'hash'
});

export const currentHash: ValidatorFn<string> = hash => val => ({
  isValid: !val || val === hash,
  message: 'Invalid current hash',
  type: 'current-hash'
});

export const percent: Validator = val => ({
  isValid: !val || (Number(val) >= 0 && Number(val) <= 100),
  message: 'Invalid percentage value',
  type: 'percent'
});

export const futureDate: Validator = val => ({
  isValid: !val || new Date(val.toString()) > new Date(),
  message: 'Invalid future date',
  type: 'future-date'
});

export function getValidatorValue<T extends ValidatorValue> (raw: T | ((form: unknown) => T), form: unknown): T {
  return typeof raw === 'function' ? raw(form) : raw;
}
