import { BigNumber } from 'bignumber.js';

export function toBigNumber (value: BigNumber.Value): BigNumber {
  return new BigNumber(value);
}
