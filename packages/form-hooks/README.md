# Q Form Hooks

A set of form hooks with validation for Q Blockchain frontend projects.

<div>
  
  [![npm](https://img.shields.io/npm/v/@q-dev/form-hooks?color=ca0001)](https://www.npmjs.com/package/@q-dev/form-hooks)
  [![npm min zipped size](https://img.shields.io/bundlephobia/minzip/@q-dev/form-hooks)](https://bundlephobia.com/package/@q-dev/form-hooks@1.4.0)
  [![license](https://img.shields.io/npm/l/@q-dev/form-hooks)](https://gitlab.com/q-dev/q-web-kit/-/blob/main/LICENSE)
  [![pipeline status](https://gitlab.com/q-dev/q-web-kit/badges/main/pipeline.svg)](https://gitlab.com/q-dev/q-web-kit/-/commits/main)
</div>

## Installation

To install the library, run the following command:

```bash
yarn add @q-dev/form-hooks
```

or

```bash
npm install @q-dev/form-hooks
```

### Requirements

* [react ^17.0.1](https://npmjs.com/package/react)

## Usage

### Form with validation

```tsx
import { useForm, required } from '@q-dev/form-hooks';

const form = useForm({
  initialValues: {
    name: '',
    email: '',
    password: '',
  },
  validators: {
    name: [],
    email: [required],
    password: [required],
  },
  onSubmit: (values) => {
    console.log(values);
  },
});
```

### Custom validation

```tsx
import { useForm, required } from '@q-dev/form-hooks';

const form = useForm({
  initialValues: {
    name: '',
    email: '',
    password: '',
  },
  validators: {
    name: [],
    email: [required],
    password: [
      required,
      val => ({
        isValid: !val || PASSWORD_REGEX.test(String(val)),
        message: 'Invalid password',
      })
    ],
  },
  onSubmit: (values) => {
    console.log(values);
  },
});
```

### Translations

Translations can be provided with `type` property from `validator` object. Types for each validator can be found [here](./src/validators/index.ts).

```tsx
import { useForm, required } from '@q-dev/form-hooks';

const form = useForm({
  initialValues: { name: '' },
  validators: { name: [] },
  onSubmit: (values) => {
    console.log(values);
  }
});

const { t } = useTranslation();
const translateError = (type: string) => {
  switch (type) {
    case 'required':
      return t('validation.required');
    default:
      return error;
  }
};
```

### JSX

```tsx
import { useForm, required } from '@q-dev/form-hooks';

const form = useForm({
  initialValues: { name: '' },
  validators: { name: [] },
  onSubmit: (values) => {
    console.log(values);
  }
});

return (
  <form onSubmit={form.submit}>
    <input
      type="text"
      name="name"
      value={form.values.name}
      onChange={e => form.fields.name.onChange(e.target.value)}
    />
    {form.errors.name && <span>{form.errors.name}</span>}
    {/* or with Q UI Kit input */}
    {/* https://gitlab.com/q-dev/q-web-kit/-/blob/main/packages/ui-kit/src/components/Input/Input.tsx */}
    <Input {...form.fields.name} />
    <button type="submit">Submit</button>
  </form>
);
```

##  Contribution

We welcome contributions to the library. If you would like to submit a pull request, please make sure to follow our code style.

## Code of Conduct

This project and everyone participating in it is governed by the
[Q Form Hooks Code of Conduct](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code.

## Resources

* [Changelog](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CHANGELOG.md)
* [Contributing Guide](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CONTRIBUTING.md)

## License

[LGPL-3.0](https://gitlab.com/q-dev/q-web-kit/-/blob/main/LICENSE)
