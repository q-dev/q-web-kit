# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0-rc.12] - 2024-05-23
### Added
- `ui-kit`: `Input` and `Textarea` cursor(caret) tracking, and prevent from jumping

## [1.0.0-rc.11] - 2024-04-01
### Fixed
- `ui-kit`: `Search` props type

## [1.0.0-rc.10] - 2024-03-28
### Added
- `form-hooks`: `email` validator

## [1.0.0-rc.9] - 2024-03-19
### Fixed
- `form-hooks`: return functions in hooks `useFormArray` and `useMultiStepForm`

## [1.0.0-rc.8] - 2023-12-25
### Added
- `ui-kit`: `xTablet` breakpoint to `Breakpoints` for `media`

## [1.0.0-rc.7] - 2023-12-15
### Changed
- `ui-kit`: `title` props type in `Modal` to `ReactNode`

## [1.0.0-rc.6] - 2023-08-28
### Added
- `ui-kit`: `discord` icon

## [1.0.0-rc.5] - 2023-08-22
### Fixed
- `ui-kit`: `Input` and `Textarea` disabled color on Safari

## [1.0.0-rc.4] - 2023-08-16
### Added
- `ui-kit`: `pre-line` global class

### Changed
- `ui-kit`: `Radio`, `Input`, `Textarea` label type to `ReactNode`

## [1.0.0-rc.3] - 2023-07-28
### Added
- `form-hooks`: `initCount` argument on `useFormArray`

## [1.0.0-rc.2] - 2023-07-25
### Added
- `ui-kit`: new icons `file-pdf` and `file-docx`

### Changed
- `ui-kit`: use TS file for icons instead of JSON

### Fixed
- `ui-kit`: icon types

## [1.0.0-rc.1] - 2023-07-19
### Added
- Optional argument into `validateField` and `validateByKey` on `form-hooks`

## 1.0.0-rc.0 - 2023-06-29
### Added
- [js-utils](https://gitlab.com/q-dev/front-end-tools/q-js-utils) package
- [ui-kit](https://gitlab.com/q-dev/front-end-tools/q-ui-kit) package
- [changelog-cli](https://gitlab.com/q-dev/front-end-tools/changelog-cli) package
- [form-hooks](https://gitlab.com/q-dev/front-end-tools/q-form) package
- [react-hooks](https://gitlab.com/q-dev/front-end-tools/q-react-hooks) package
- Builder for all packages

[Unreleased]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.12...main
[1.0.0-rc.12]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.11...v1.0.0-rc.12
[1.0.0-rc.11]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.10...v1.0.0-rc.11
[1.0.0-rc.10]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.9...v1.0.0-rc.10
[1.0.0-rc.9]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.8...v1.0.0-rc.9
[1.0.0-rc.8]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.7...v1.0.0-rc.8
[1.0.0-rc.7]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.6...v1.0.0-rc.7
[1.0.0-rc.6]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.5...v1.0.0-rc.6
[1.0.0-rc.5]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.4...v1.0.0-rc.5
[1.0.0-rc.4]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.3...v1.0.0-rc.4
[1.0.0-rc.3]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.2...v1.0.0-rc.3
[1.0.0-rc.2]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.1...v1.0.0-rc.2
[1.0.0-rc.1]: https://gitlab.com/q-dev/q-web-kit/compare/v1.0.0-rc.0...v1.0.0-rc.1
