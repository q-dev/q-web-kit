const fs = require('fs');
const path = require('path');
const chalk = require('chalk');

const ORGANIZATION_NAME = '@q-dev';
const VERSION_REGEX = /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/;
const VERSION = require('../package.json').version;

const issuesFound = [];
const packages = fs.readdirSync(path.resolve(__dirname, '../packages'));

const run = () => {
  validateSemverCompatibility();

  if (!issuesFound.length) applyVersion();
  report();
};

function validateSemverCompatibility () {
  if (!VERSION) {
    issuesFound.push('No version provided');
  }

  if (!VERSION_REGEX.test(VERSION)) {
    issuesFound.push(`Version ${VERSION} not semver compatible, should match https://semver.org/ specification`);
  }
}

function applyVersion () {
  for (const pkg of packages) {
    const packageJsonPath = path.resolve(__dirname, `../packages/${pkg}/package.json`);
    const packageJson = require(packageJsonPath);

    if (packageJson.version === VERSION) {
      continue;
    }

    packageJson.version = VERSION;

    fs.writeFile(packageJsonPath, `${JSON.stringify(packageJson, null, 2)}\n`, (err) => {
      if (err) issuesFound.push(`[${ORGANIZATION_NAME}/${pkg}]: Version apply failed: ${err.toString()}`);
    });

    if (issuesFound.length) break;
  }
}

function report () {
  if (issuesFound.length) {
    console.error(chalk`{red Version {yellow ${VERSION}} apply failed!}`);
    issuesFound.forEach((issue) => {
      console.error(chalk`{red ${issue}}`);
    });
    process.exitCode = 1;
  } else {
    /* eslint-disable-next-line no-console */
    console.log(chalk`{green Version {yellow ${VERSION}} apply passed}`);
    packages.forEach((pkg) => {
      console.error(chalk`{yellow [${ORGANIZATION_NAME}/${pkg}]}: {green ${VERSION}}`);
    });
    process.exitCode = 0;
  }
}

run();
