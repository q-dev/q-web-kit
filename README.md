## Table of Contents

- [Table of Contents](#table-of-contents)
- [Changelog](#changelog)
- [Packages](#packages)
- [Development](#development)
  - [Dependencies](#dependencies)
    - [Local dependencies](#local-dependencies)
    - [Testing dependencies](#testing-dependencies)
  - [Basics](#basics)
    - [Build](#build)
    - [Run tests](#run-tests)
    - [Run linter](#run-linter)
    - [Bump version for all packages](#bump-version-for-all-packages)
- [Contribute](#contribute)
  - [Reporting Issues](#reporting-issues)
- [License](#license)
- [Resources](#resources)

## Changelog

For the change log, see [CHANGELOG.md](https://gitlab.com/q-dev/q-web-kit/-/blob/main/CHANGELOG.md).

## Packages

The Q Development AG Web-Kit is a library that consists of many smaller NPM packages within the
[@q-dev namespace](https://www.npmjs.com/org/q-dev), a so-called monorepo.

Here are the packages in the namespace:

| Package                                                                                                | Description                                                                  | Latest                                                                                                                      |
|--------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
| [@q-dev/q-ui-kit](https://gitlab.com/q-dev/q-web-kit/-/tree/init-monorepo/packages/ui-kit)             | A set of React UI components for Q Blockchain                                | [![npm version](https://img.shields.io/npm/v/@q-dev/q-ui-kit.svg)](https://www.npmjs.com/package/@q-dev/q-ui-kit)       |
| [@q-dev/react-hooks](https://gitlab.com/q-dev/q-web-kit/-/tree/init-monorepo/packages/react-hooks)     | A set of React hooks for Q Blockchain frontend projects.                     | [![npm version](https://img.shields.io/npm/v/@q-dev/react-hooks.svg)](https://www.npmjs.com/package/@q-dev/react-hooks)     |
| [@q-dev/form-hooks](https://gitlab.com/q-dev/q-web-kit/-/tree/init-monorepo/packages/form-hooks)       | A set of React form hooks with validation for Q Blockchain frontend projects | [![npm version](https://img.shields.io/npm/v/@q-dev/form-hooks.svg)](https://www.npmjs.com/package/@q-dev/form-hooks)       |
| [@q-dev/utils](https://gitlab.com/q-dev/q-web-kit/-/tree/init-monorepo/packages/js-utils)              | A set of JavaScript utils for Q Blockchain frontend projects                 | [![npm version](https://img.shields.io/npm/v/@q-dev/utils.svg)](https://www.npmjs.com/package/@q-dev/utils)                 |
| [@q-dev/changelog-cli](https://gitlab.com/q-dev/q-web-kit/-/tree/init-monorepo/packages/changelog-cli) | CLI tool for [changelog](https://keepachangelog.com/en/1.0.0/) automation    | [![npm version](https://img.shields.io/npm/v/@q-dev/changelog-cli.svg)](https://www.npmjs.com/package/@q-dev/changelog-cli) |

## Development

### Dependencies

#### Local dependencies

To install all dependencies, run:
```bash
yarn install
```

If you are implementing a new package which needs to depend on the local package, you can use the following command to install it:
```bash
yarn workspace @q-dev/target-package add  @q-dev/package-to-add
```

To install a dependency to all packages, use the following command:
```bash
yarn workspaces foreach -pt run add @q-dev/package-to-add
```

#### Testing dependencies

To test the packages, you need:

1. Build the packages:

    ```bash
    yarn build
    ```
2. Switch yarn to version berry in the project where you want to test package, to yarn be able to resolve workspace dependencies:

    ```bash
    yarn set version berry
    ```
3. Add this to the `.yarnrc.yml` file:

    ```yaml
    nodeLinker: node-modules
    ```
4. Link the packages to the project:

    ```bash
    yarn link -p -A /path/to/web-kit/root/directory
    ```
5. Add dependencies to the package.json file:

    ```json
    {
      "dependencies": {
        "@q-dev/utils": "*"
      }
    }
    ```

6. Install the dependencies:
    ```bash
    yarn install
    ```

### Basics

#### Build

```bash
yarn build
```

#### Run tests

```bash
yarn test
```

#### Run linter

```bash
yarn lint
```

#### Bump version for all packages

```bash
npm version minor
```

## Contribute

First off, thanks for taking the time to contribute!
Now, take a moment to be sure your contributions make sense to everyone else.

### Reporting Issues

Found a problem? Want a new feature? First of all see if your issue or idea has [already been reported](https://gitlab.com/q-dev/q-web-kit/-/issues).
If don't, just open a [new clear and descriptive issue](https://gitlab.com/q-dev/q-web-kit/-/issues/new).

## License

[LGPL-3.0](https://gitlab.com/q-dev/q-web-kit/-/blob/main/LICENSE)

## Resources
- [Yarn Berry](https://yarnpkg.com/cli/install)
- [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
- [Semantic Versioning](https://semver.org/spec/v2.0.0.html)
